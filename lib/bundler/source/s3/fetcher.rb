# frozen_string_literal: true

require 'digest/md5'
require 'pathname'

require 'aws-sdk-s3'

module Bundler
  class Source
    class S3
      # Class for fetching objects from S3 bucket and storing them to local filesytem
      class Fetcher
        def initialize(bucket_name:)
          @bucket = Aws::S3::Bucket.new(name: bucket_name)
        end

        attr_reader :bucket
        private :bucket

        def list(prefix: nil)
          bucket.objects(prefix: prefix).map(&:key)
        end

        # Fetch an S3 object specified by the given key from the bucket and store it to the given path
        #
        # @param [String] key The object's key
        # @param [Pathname] root Local root directory to store fetched content
        # @return [Pathname] Path of the fetched file(= root + key)
        def fetch(key, root:)
          (root + key).tap do |path|
            break path if path.exist? && same?(key, path)
            path.dirname.mkpath unless path.dirname.exist?
            remote_object(key).get(response_target: path)
          end
        end

        def same?(key, path)
          remote_digest(key) == local_digest(path)
        end

        def local_digest(path)
          Digest::MD5.hexdigest(path.binread)
        end

        def remote_digest(key)
          remote_object(key).etag.delete(?")
        end

        def remote_object(key)
          bucket.object(key)
        end
      end
    end
  end
end
