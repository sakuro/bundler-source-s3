# frozen_string_literal: true

require 'bundler/source/s3/version'
require 'bundler/source/s3/fetcher'

require 'bundler/source'
require 'bundler/plugin/api/source'

require 'zlib'

module Bundler
  class Source
    # This class adds support for fetching gems from Amazon S3 to bundler
    class S3
      def initialize(options)
        super

        @bucket_name = options['uri']
        @fetcher = Bundler::Source::S3::Fetcher.new(bucket_name: @bucket_name)
      end

      attr_reader :fetcher, :bucket_name
      private :fetcher, :bucket_name

      def fetch_gemspec_files
        specs_gz_key = specs_key
        specs_gz_path = fetcher.fetch(specs_gz_key, root: tmp)
        specs = load_specs(specs_gz_path)
        specs.each do |spec|
          full_name = '%s-%s' % spec
          gemspec_key = gemspec_key(full_name)
          gemspec_path = fetcher.fetch(gemspec_key, root: tmp)
          cache_gemspec(full_name, gemspec_path)
        end
        cache_dir.glob('**/*.gemspec').map(&:to_s)
      end

      def install(spec, options)
        gem_key = gem_key(spec.full_name)
        gem_path = fetcher.fetch(gem_key, root: tmp)
        system 'gem', 'install', '--local', gem_path.to_s
      end

      def dependency_names_to_double_check
        dependency_names
      end

      private

      def api
        @api = Bundler::Plugin::API.new
      end

      def tmp
        @tmp ||= api.tmp
      end

      def cache_dir
        api.cache_dir + 's3' + bucket_name
      end

      def gemspec_cache_path(full_name)
        cache_dir + 'specifications' + '%s.gemspec' % full_name
      end

      def gem_cache_path(full_name)
        cache_dir + 'gems' + '%s.gem' % full_name
      end

      # Inflate given gemspec and cache it
      def cache_gemspec(full_name, gemspec_rz_path)
        data = gemspec_rz_path.binread
        raise 'Not deflated: %s' % gemspec_rz_path unless deflated?(data)
        gemspec_cache_path = gemspec_cache_path(full_name)
        gemspec_cache_path.dirname.mkpath unless gemspec_cache_path.dirname.exist?
        gemspec = Marshal.load(Zlib.inflate(data))
        gemspec_cache_path.write(gemspec.to_ruby)
      end

      # Return array of specs.
      # @return [Array] Array of specs.  Each element is in form: [name, Gem::Version, platform]
      def load_specs(specs_path)
        data = specs_path.binread
        if gzipped?(data)
         Marshal.load(Zlib.gunzip(data))
        else
          Marshal.load(data)
        end
      end

      GZIP_MAGIC = "\x1F\x8B".b

      def gzipped?(bin)
        bin[0,2] == GZIP_MAGIC
      end

      # RFC6713 and magic(5)
      # 0       string/b        x
      # >0      beshort%31      =0
      # >>0     byte&0xf        =8
      # >>>0    byte&0x80       =0
      def deflated?(bin)
        test1, test2, test3 = *bin.unpack('@0a @0n @0C')
        test1 == ?x && test2 % 31 == 0 && test3 & 0x0F == 8 &&  test3 & 0x80 == 0
      end

      def gem_key(full_name)
        'gems/%s.gem' % full_name
      end

      def gemspec_key(full_name)
        'quick/Marshal.%s/%s.gemspec.rz' % [Gem.marshal_version, full_name]
      end

      def specs_key(prefix: '', gz: true)
        ('specs.%s' % Gem.marshal_version).tap do |key|
          key.prepend(prefix + '_') if prefix.size > 0
          key += '.gz' if gz
        end
      end
    end
  end
end

Bundler::Plugin::API.source('s3', Bundler::Source::S3)
